#include "MemoryLeakCheck.h"
#include <string>

extern "C" void C_leak_test();
void CPP_leak_test();

int main()
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    CPP_leak_test();
    C_leak_test();

    // NOTE: Not required when _CRTDBG_LEAK_CHECK_DF is used,
    // and will be called automatically on exit of main.

    //_CrtDumpMemoryLeaks();
}
