#ifndef OCL_MEMORYLEAKCHECK_H
#define OCL_MEMORYLEAKCHECK_H

#ifdef _DEBUG

// Enable the malloc/free and new/delete leak detection.
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifdef __cplusplus
// Enable memory leak checking for new/delete to show filename with line number.
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif /* __cplusplus */

#endif /* _DEBUG */

#endif /* OCL_MEMORYLEAKCHECK_H */
