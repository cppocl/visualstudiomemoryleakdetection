#include "MemoryLeakCheck.h"
#include <string.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

void C_leak_test()
{
    char* pstr_leaking = NULL;
    pstr_leaking = (char*)malloc(6);
    memcpy(pstr_leaking, "abcde", 6);
}

#ifdef __cplusplus
} /* extern "C" */
#endif
